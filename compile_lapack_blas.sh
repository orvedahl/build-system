#!/bin/sh -p
#
#
# generate the lapack and blas libraries
#

# get current directory
pwd=`pwd`

# change to the Lapack directory
cd $1

echo
echo "Building LAPACK and BLAS"
echo
# make the libraries
make

# go back to original directory
cd $pwd

echo
echo "COMPLETE"
echo
