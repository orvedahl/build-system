#
# MPI configuration
#

ifndef MPI
$(error THIS FILE SHOULD ONLY BE LOADED WITH MPI:=t)
endif

#----------------------------------------------------------------------
# Architecture specific changes
#----------------------------------------------------------------------
ifeq ($(arch), Linux)

  # If using a locally installed mpif90, this points to the executable
  ifdef MPIHOME

    f90_comp = $(MPIHOME)/bin/mpif90

  else
    ifeq ($(f90_comp),gfortran)

      f90_comp = mpif90

    else
      ifeq ($(f90_comp), intel)

        f90_comp = mpiif90

      else

        $(error Compiler not yet supported for MPI ... check GMainMPI.mak)

      endif

    endif

  endif

endif

# Mac support
ifeq ($(arch), Darwin)

  ifeq ($(f90_comp),gfortran)

    f90_comp = mpif90

  else
    ifeq ($(f90_comp), intel)

      f90_comp = mpiif90

    else
      # attempt to have general support for Mac, only works if MPIHOME env var
      # is set and applies some general defaults
      ifdef MPIHOME

        f90_comp = $(MPIHOME)/bin/mpif90

      else

        $(error Compiler not yet supported for MPI ... check GMainMPI.mak)

      endif

    endif

  endif

endif
