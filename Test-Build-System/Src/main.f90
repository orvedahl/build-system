!
! main driver program to test the multi-dimensional integral codes
!
module function_module

   use data_types
   use variables

   implicit none

   contains

   !=====================================
   ! random number generator
   !=====================================
   function random_num(lo, hi) result (r)

      real(kind=dpt) :: r, lo, hi

      call RANDOM_NUMBER(r)

      ! convert from r = [0,1] --> r = [lo,hi]
      r = r*(hi - lo) + lo

      return

   end function random_num

   !=====================================
   ! function integrand
   !=====================================
   function func(x, y) result (f)

      real(kind=dpt) :: x, y, f, coeff

      !f = -x**2 - y**2
      f = cos(y)**2

      ! add the volume element corrections
      !      solid angle: dOmega = sin(theta) d(theta) d(phi) = dA/r**2
      !                   coeff = sin(theta)
      !      cartesian  : dA = dx dy
      !                   coeff = one
      !      cylindrical: dA = r d(theta) dr
      !                   coeff = r
      !      spherical  : dA = r**2 sin(theta) d(theta) d(phi)
      !                   coeff = r**2 sin(theta)

      coeff = sin(y)

      f = coeff*f

      return

   end function func

   !=====================================
   ! true value of integral
   !=====================================
   function true(xlo, xhi, ylo, yhi) result (I)

      real(kind=dpt) :: xlo, xhi, ylo, yhi, I

      !I = -third*(xhi**3-xlo**3)*(yhi-ylo) - third*(yhi**3-ylo**3)*(xhi-xlo)
      I = twopi*third

      return

   end function true

   !=====================================
   ! 2-d integral subroutine
   !=====================================
   subroutine two_d_integral(xlo, xhi, ylo, yhi, N, Integral, dI)
   
      ! INPUT/OUTPUT
      real(kind=dpt), intent(in) :: xlo, ylo   ! lower bounds
      real(kind=dpt), intent(in) :: xhi, yhi   ! upper bounds
      integer, intent(in) :: N                 ! number of points
      real(kind=dpt), intent(out) :: Integral, dI   ! result and error

      ! LOCAL
      real(kind=dpt) :: avg_f, avg_f2, min_f, max_f, f
      real(kind=dpt) :: x, y, z, vol
      integer :: i, cnt, ncnt, hits

      !===============================================================
      ! calculate the volume (necessary only if complicated boundaries
      ! find max/min value of function
      !max_f = zero
      !min_f = zero
      !do i=1,N
         
         ! get random numbers
      !   x = random_num(xlo, xhi)
      !   y = random_num(ylo, yhi)

      !   f = func(x, y)

      !   if (f > max_f) then
      !      max_f = f
      !   elseif (f < min_f) then
      !      min_f = f
      !   endif

      !enddo

      !cnt = 0   ! how many pnts hit a positive area
      !ncnt = 0  ! how many pnts hit a negative area
      !do i=1,N

         ! get random numbers
      !   x = random_num(xlo, xhi)
      !   y = random_num(ylo, yhi)
      !   z = random_num(min_f, max_f)

      !   f = func(x, y)

      !   if (z >= zero .and. z < f) then
            ! pnt is between graph and axis, with positive area
      !      cnt = cnt + 1

      !   elseif (z <= zero .and. z > f) then
            ! pnt is between graph and axis, with negative area
      !      ncnt = ncnt + 1

      !   elseif (z == f) then
            ! pnt is on curve
      !      cnt = cnt + 1

      !   endif

      !enddo

      ! total area (hits/N) = positive area (cnt) - negative area (ncnt)
      !hits = cnt + ncnt

      ! calc volume = fraction under curve times vol of cube bounded by curve
      !vol = real(hits, kind=dpt)/real(N, kind=dpt) * &
      !      (xhi - xlo) * (yhi-ylo) * abs(max_f - min_f)
      !===============================================================

      ! volume is easy in 2-D
      vol = (xhi - xlo) * (yhi - ylo)

      ! calc averages
      ! PARALLEL PRIVATE (x, y) REDUCTION( + : avg_f, avg_f2 )
      !$OMP PARALLEL REDUCTION( + : avg_f, avg_f2 )
      avg_f = zero
      avg_f2 = zero
      !$OMP DO
      do i=1, N

         ! get random numbers
         x = random_num(xlo, xhi)
         y = random_num(ylo, yhi)

         f = func(x, y)

         ! get avg of f and avg of f**2
         avg_f  = avg_f + f
         avg_f2 = avg_f2 + f*f

      enddo
      !$OMP END PARALLEL

      avg_f = avg_f/real(N, kind=dpt)
      avg_f2 = avg_f2/real(N, kind=dpt)

      Integral  = vol*avg_f
      if (avg_f2 > avg_f) then
         dI = vol * dsqrt((avg_f2 - avg_f)/real(N, kind=dpt))
      else
         dI = vol * dsqrt((avg_f - avg_f2)/real(N, kind=dpt))
      endif

      return
      
   end subroutine two_d_integral

end module function_module

!=====================================================
! main
!=====================================================
program main

   use data_types
   use variables
   use function_module
   use input_params
   use omp_module

   implicit none

   real(kind=dpt) :: xlo, xhi, ylo, yhi, true_sol, I, dI, start, endtime

   ! get runtime params
   call runtime_init()

   ! lower bounds
   xlo = zero
   ylo = zero

   ! upper bounds
   xhi = twopi
   yhi = half*pi

   ! start omp clock
   start = omp_get_wtime()

   ! get numerical integral
   call two_d_integral(xlo, xhi, ylo, yhi, N, I, dI)

   ! get exact solution
   true_sol = true(xlo, xhi, ylo, yhi)   

   ! start omp clock
   endtime = omp_get_wtime()

   ! output the answers
   101 format (a,3x,f12.8)
   102 format (a,3x,f12.8,1x,a,1x,f12.8)
   print *,''
   print 102 ,'Numerical Integral: ', I,'+/-', dI
   print 101,'True Value of Int : ',true_sol
   print 101,'    Abs Error     : ',abs(true_sol - I)
   print 101,'       Time       : ',endtime-start
   print *,''
   call write_job_info(".")
   print *,  '   Job Info File  :  '//trim("."//"/job_info")
   print *,''

end program main

