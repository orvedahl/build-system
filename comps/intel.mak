#
# makefile rules surrounding Intel compiler
#

# get version
f90_comp := ifort
f90_comp_vers := $(shell $(f90_comp) -V 2>&1 | grep 'Version')

intel_dbg = -FR -r8 -O0 -traceback -g -check all -noinline -debug all -warn all -shared-intel

# set debugging flags
ifdef debug
  f90_flags := $(intel_dbg)
else
  f90_flags := -FR -r8 -O3 -shared-intel
endif

# we are compiling CSS Code
ifdef CSS_Code

  ifeq ($(DOPTS),1)
    f90_flags += -xSSE4.2 -DVECWIDTH=16 -align rec16byte
    arc_suf := .wes

  else ifeq ($(DOPTS),2)
    f90_flags += -xSSE4.1 -DVECWIDTH=16 -align rec16byte
    arc_suf := .har

  else ifeq ($(DOPTS),3)
    f90_flags += -xSSE2 -DVECWIDTH=16 -align rec16byte
    arc_suf := .sse2

  else ifeq ($(DOPTS),4)
    f90_flags += -xAVX -DVECWIDTH=32 -align rec32byte
    arc_suf := .san

  else
    f90_flags = $(intel_dbg)
    arc_suf := .err

  endif

endif

# set OMP flags
ifdef OMP
  f90_flags += -openmp
endif

# set MKL flags
ifdef Intel_MKL
  f90_flags += -mkl=sequential
endif

f90_compile = $(f90_flags) -module $(mdir) -I$(mdir)
f90_link = $(f90_flags) -module $(mdir) -I$(mdir)

