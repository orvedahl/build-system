#
# Main makefile build Information
#

#----------------------------------------------------------------
# remove command
#----------------------------------------------------------------
RM := -rm

#----------------------------------------------------------------
# pretty output shortcuts
#----------------------------------------------------------------
bold   = `tput bold`
normal = `tput sgr0`
red    = `tput setaf 1`
green  = `tput setaf 2`
yellow = `tput setaf 3`
blue   = `tput setaf 4`
purple = `tput setaf 5`
cyan   = `tput setaf 6`
gray   = `tput setaf 7`

#----------------------------------------------------------------
# machine info
#----------------------------------------------------------------
arch := $(shell uname)
unamen := $(shell uname -n)
hostnamef := $(shell hostname -f)

#----------------------------------------------------------------
# include compiler info
#----------------------------------------------------------------
compiler_directory := $(makefile_directory)/comps

ifeq ($(findstring gfortran, $(f90_comp)),gfortran)
  include $(compiler_directory)/gfortran.mak
  comp_suf := .gfortran
else
  ifeq ($(f90_comp), intel)
    include $(compiler_directory)/intel.mak
    comp_suf := .intel
  else
    $(error "Compiler $(f90_comp) is not supported")
  endif
endif

ifdef MPI
  include $(makefile_directory)/GMainMPI.mak
endif

#----------------------------------------------------------------
# suffix info/define main executable
#----------------------------------------------------------------
ifdef MPI
  mpi_suf := .mpi
endif
ifdef OMP
  omp_suf := .omp
endif
ifdef debug
  debug_suf := .debug
endif

suf=$(arch)$(comp_suf)$(debug_suf)$(omp_suf)$(mpi_suf)$(arc_suf)

ifdef exe_base
  base := $(exe_base)
else
  base := main
endif

exe=x$(base).$(suf).exe

#----------------------------------------------------------------
# define output directories
#----------------------------------------------------------------
tname = _build
tdir = $(tname)/$(suf)
odir = $(tdir)/o
mdir = $(tdir)/m

#----------------------------------------------------------------
# find/include the GPackage.mak files
#----------------------------------------------------------------
f90sources =
GPack_fil :=
vpath_loc :=

#----------------------------------------------------------------
# python dependency checker info
#----------------------------------------------------------------
dep_script := $(makefile_directory)/dependencies.py

# output file
dep_file := $(tdir)/fortran.depends

#----------------------------------------------------------------
# what will be made
#----------------------------------------------------------------
ifdef use_lapack
all: build_lapack $(dep_file) $(exe)
else
all: $(dep_file) $(exe)
endif

# add additional cleanup if using lapack
ifdef use_lapack
realclean::
	cd $(lapack_loc);$(MAKE) cleanall

clean::
	cd $(lapack_loc);$(MAKE) clean
endif

#----------------------------------------------------------------
# how to build lapack
#----------------------------------------------------------------
build_lapack:
	$(makefile_directory)/compile_lapack_blas.sh $(lapack_loc)

#----------------------------------------------------------------
# runtime parameter stuff (probin.f90)
#----------------------------------------------------------------
probin_template := $(makefile_directory)/input_params_template

# directories to search for the _params files
probin_dirs = .
probin_dirs += $(src_dirs)
probin_dirs += $(xtr_src_dirs)

# get list of all valid _params files
params_files := $(shell $(makefile_directory)/find_paramfiles.py $(probin_dirs))

probin.f90: $(params_files) $(probin_template)
ifdef verbose
	@echo ""
	@echo "${bold}Writing probin.f90 ...${normal}"
	$(makefile_directory)/write_input_params.py -t "$(probin_template)" \
             -o probin.f90 -n input -p "$(params_files)"
	@echo ""
else
	$(makefile_directory)/write_input_params.py -t "$(probin_template)" \
             -o probin.f90 -n input -p "$(params_files)"
endif

clean::
	$(RM) -f probin.f90

#----------------------------------------------------------------
# build info stuff (build_info.f90)
#----------------------------------------------------------------
$(dep_file): build_info.f90

build_info.f90:
ifdef verbose
	@echo ""
	@echo "${bold}Writing build_info.f90 ...${normal}"
	$(makefile_directory)/makebuildinfo.py \
           --FCOMP "$(f90_comp)" \
           --FCOMP_version "$(f90_comp_vers)" \
           --f90_compile_line "$(f90_comp) $(f90_compile) -c" \
           --link_line "$(f90_comp) $(f90_link)" \
           --source_home "$(src_dirs) $(xtr_src_dirs)"
	@echo ""
else
	$(makefile_directory)/makebuildinfo.py \
           --FCOMP "$(f90_comp)" \
           --FCOMP_version "$(f90_comp_vers)" \
           --f90_compile_line "$(f90_comp) $(f90_compile) -c" \
           --link_line "$(f90_comp) $(f90_link)" \
           --source_home "$(src_dirs) $(xtr_src_dirs)"
endif

f90sources += build_info.f90

$(odir)/build_info.o: build_info.f90
	@if [ ! -d $(odir) ]; then mkdir -p $(odir); fi
	@if [ ! -d $(mdir) ]; then mkdir -p $(mdir); fi
ifdef verbose
	@echo "${bold}Building $< ...${normal}"
	$(f90_comp) $(f90_compile) -c $< -o $@
	$(RM) -f build_info.f90
else
	$(f90_comp) $(f90_compile) -c $< -o $@
	$(RM) -f build_info.f90
endif

clean::
	$(RM) -f build_info.f90

#----------------------------------------------------------------
# find/include the GPackage.mak files and add locations to vpath
#----------------------------------------------------------------
# main src directory
GPack_fil += $(foreach dir, $(src_dirs), $(dir)/GPackage.mak)
vpath_loc += $(foreach dir, $(src_dirs), $(dir))

# other src directories
GPack_fil += $(foreach dir, $(xtr_src_dirs), $(dir)/GPackage.mak)
vpath_loc += $(foreach dir, $(xtr_src_dirs), $(dir))

# did not find any GPackage.mak files
ifndef GPack_fil
  ifneq ($(MAKECMDGOALS), realclean)
    ifneq ($(MAKECMDGOALS), clean)
      GPack_err = "No GPackage.mak found: Set the src_dirs/xtr_src_dirs vars"
      $(error $(GPack_err))
    endif
  endif
endif

include $(GPack_fil)

#----------------------------------------------------------------
# get object/source files
#    sf90sources will not go through the dependency checker
#----------------------------------------------------------------
objects = $(addprefix $(odir)/, $(sort $(f90sources:.f90=.o)))
objects += $(addprefix $(odir)/, $(sort $(sf90sources:.f90=.o)))

vpath %.f90 . $(vpath_loc)

#----------------------------------------------------------------
# rule to build the dependency file
#    The magic happens with the '$^' character. This holds all 
#    the dependencies with their full directory path
#----------------------------------------------------------------
$(dep_file): $(sort $(f90sources))
	@if [ ! -d $(tdir) ]; then mkdir -p $(tdir); fi
ifdef verbose
	@echo ""
	@echo "${bold}Writing f90 dependency File ...${normal}"
	$(dep_script) --no-msgs --output=$(dep_file) \
		--skip-mods="$(skip_modules)" --prefix=$(odir)/ $^
	@echo ""
else
	$(dep_script) --no-msgs --output=$(dep_file) \
		--skip-mods="$(skip_modules)" --prefix=$(odir)/ $^
endif

# include the dependencies file (which says what depends on what)
# MAKECMDGOALS is an intrinsic GNUmakefile variable that holds what 
# target was entered on the command line
# Only include the depends file if we are not cleaning up
ifneq ($(MAKECMDGOALS), realclean)
  ifneq ($(MAKECMDGOALS), clean)
    include $(dep_file)
  endif
endif

#----------------------------------------------------------------
# build executable
#----------------------------------------------------------------
$(exe): $(objects)
ifdef verbose
	@echo "${bold}Linking $@ ...${normal}"
	$(f90_comp) $(f90_link) -o $(exe) $(objects) $(xtr_libs) $(xtr_inc_dirs)
	@echo
	@echo "${bold}${green}SUCCESS${normal}"
	@echo
else
	$(f90_comp) $(f90_link) -o $(exe) $(objects) $(xtr_libs) $(xtr_inc_dirs)
	@echo
	@echo "${bold}${green}SUCCESS${normal}"
	@echo
endif

#----------------------------------------------------------------
# how to build each .o file
#----------------------------------------------------------------
$(odir)/%.o: %.f90
	@if [ ! -d $(odir) ]; then mkdir -p $(odir); fi
	@if [ ! -d $(mdir) ]; then mkdir -p $(mdir); fi
ifdef verbose
	@echo "${bold}Building $< ...${normal}"
	$(f90_comp) $(f90_compile) -c $< -o $@ $(xtr_inc_dirs)
else
	$(f90_comp) $(f90_compile) -c $< -o $@ $(xtr_inc_dirs)
endif

#----------------------------------------------------------------
# cleanup
#----------------------------------------------------------------
# make will execute even if there exists files named clean, all, etc.
.PHONY: clean realclean all

clean::
	$(RM) -f ./*.o $(odir)/*.o
	$(RM) -f ./*.mod $(mdir)/*.mod
	$(RM) -f $(dep_file)

realclean:: clean
	$(RM) -rf  $(tname)
	$(RM) -f *.exe

#----------------------------------------------------------------
# debug aide
#----------------------------------------------------------------
# type "make print-f90sources" and it will print the 
#   value of the variable f90sources
print-%: ; @echo $* is $($*)


