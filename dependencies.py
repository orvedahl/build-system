#!/usr/bin/env python
#
# Python Script to generate a list of dependencies for use in a makefile
#
# The output of this program can be included directly into the makefile
#
# 2012-08-03 R. Orvedahl
#

from __future__ import print_function
import getopt
import string
import sys
import os
import re


#####################################################################
#  Main program
#####################################################################
def main_program(output, prefix, skip_modules, files, print_msgs):

    # break skip_modules into a list and convert each element to lowercase
    skip_mods = string.split(skip_modules)
    for i in range(len(skip_mods)):
        skip_mods[i] = str(skip_mods[i]).lower()

    # find all use statements of the form:
    #    use module-name, only: var1, var2
    use_re = re.compile("(\s*)(use)(\s+)((?:[a-z_][a-z_0-9]+))",
                        re.IGNORECASE|re.DOTALL)
           # (\s+)  = 1 or more white space
           # (\s*)   = 0 or more spaces
           # (use)  = 'use'
           # ((?:[a-z_][a-z_0-9]+)) = pretty much any fortran acceptable
           #                           variable name
           #    ?: = matches whatever follows, but cant be stored unless
           #           the entire re.compile() is set equal to something.
           #           This is more efficient than searching everytime
           #    [a-z_] = any letter plus the '_'
           #    [a-z_0-9] = any letter, number or '_'
           #    following '+' = results in 1 or more repetions of what 
           #                      came before it (so it can match words)
           #    IGNORECASE = ignores case
           #    | seperator = acts as a logical OR
           #    DOTALL = allows the '.' character, which usually only matches
           #               everything except newlines, to include newlines

    # find all modules of the form:
    #    module module-name
    module_re = re.compile("(\s*)(module)(\s+)((?:[a-z_][a-z_0-9]+))",
                        re.IGNORECASE|re.DOTALL)

    # need to exclude the possible:
    #    module procedure
    # that appears inside an interface statement
    module_procedure_re = \
      re.compile("(\s*)(module)(\s+)(procedure)(\s+)((?:[a-z_][a-z_0-9]+))",
                        re.IGNORECASE|re.DOTALL)

    # use a dictionary to store the module statements:
    #    'module-name':filename
    modulefiles = {}

    # parse all files and look for module statements
    # find the filename that contains a given module
    for fil in files:

        mf = open(fil,'r')
        line = mf.readline()

        while(line):

            # find comments that appear after valid code
            ind = string.find(line, "!")

            # if there is no "!", then search entire line
            if (ind < 0):
                ind = len(line)

            # skip comments and blank lines
            if (not(line.lstrip().startswith("!") or line.isspace())):

                # only need to search Fortran statements: remove comments
                #   that appear after the valid code
                line = line[:ind]
    
                # does line contain module, module procedure
                re_break  = module_re.search(line)
                re_break2 = module_procedure_re.search(line)
    
                # only include module statements
                if (re_break and not(re_break2)):
                    modulefiles[re_break.group(4)] = fil
                # module_re.search(line) returns if there is a match
                #  and what it is. This is how it is used both in the 
                #  if statement and the dictionary
                #
                # group(i) returns the ith parenthesized subgroup of the 
                #  search pattern.
                #  group(0) is entire thing
                #  here: group(1) = group(3) = space
                #        group(2) = 'use'|'module'
                #        group(4) = module-name|'procedure'
    
            line = mf.readline()


        mf.close()

    # go back through files and find use statements
    if (not os.path.exists(os.path.dirname(output))):
        os.makedirs(os.path.dirname(output))

    out = open(output,'w')

    for fil in files:

        mf = open(fil,'r')

        line = mf.readline()

        # find suffix of Fortran file:
        if (string.rfind(fil,".f90") > 0):
            suf = ".f90"
        elif (string.rfind(fil, ".f") > 0):
            suf = ".f"
        elif (string.rfind(fil, ".F") > 0):
            suf = ".F"
        elif (string.rfind(fil, ".F90") > 0):
            suf = ".F90"
        elif (string.rfind(fil, ".f95") > 0):
            suf = ".f95"
        elif (string.rfind(fil, ".F95") > 0):
            suf = ".F95"
        elif (string.rfind(fil, ".f03") > 0):
            suf = ".f03"
        elif (string.rfind(fil, ".F03") > 0):
            suf = ".F03"
        elif (string.rfind(fil, ".f08") > 0):
            suf = ".f08"
        elif (string.rfind(fil, ".F08") > 0):
            suf = ".F08"
        elif (string.rfind(fil, ".ftn") > 0):
            suf = ".ftn"
        elif (string.rfind(fil, ".FTN") > 0):
            suf = ".FTN"

        # has the actual source file been added as a dependency
        source_flag = False

        while(line):

            ind = string.find(line, "!")

            # if there is no "!", then search entire line
            if (ind < 0):
                ind = len(line)

            # skip comments and blank lines
            if (not(line.lstrip().startswith("!") or line.isspace())):

                line = line[:ind]
    
                re_break = use_re.search(line)
    
                if (not(source_flag)):
                    out.write("# "+fil +"\n")
                    out.write(prefix + \
                        os.path.basename(fil).replace(suf,".o"))
                    out.write(": " + fil + "\n")
                    source_flag = True
    
                # found a use:
                if (re_break):

                    # check that we avoid lines like:
                    #    write(*,*) 'We use the module...'
                    # otherwise it would say the file uses module 'the'
                    only_space = check_for_spaces(line, "use")
                    if (only_space):

                        # process it only if it is not in the list of 
                        # modules to skip
                        if (re_break.group(4).lower() not in skip_mods):

                            # recover file containing the module
                            try:
                                modulefiles[re_break.group(4)]

                            except KeyError:
                                sep = "   "
                                print("\n\nERROR: dependencies.py\n")
                                print(2*sep+"Modulename : "+re_break.group(4))
                                print(2*sep+"Error type : "+\
                                    "module definition "+\
                                    "not found in any file")
                                print(2*sep+\
                                    "Solution?  : check GPackage.mak for "+\
                                    "corresponding file existence")
                                sys.exit()
                            else:
                                out.write(prefix + \
                                     os.path.basename(fil).replace(suf,".o"))
                                out.write(": " + prefix + \
                                     os.path.basename(\
                                     modulefiles[\
                                     re_break.group(4)]).replace(suf,".o") \
                                     + "\n")

            line = mf.readline()

        mf.close()

    out.close()

    if (print_msgs):
        print("\n\n Added prefix to all object files: " + prefix)
        print("\n Output written to: " + output)
        print("\n\n---Complete---\n")


#####################################################################
#  Program to make sure there is only space before a use statement
#####################################################################
def check_for_spaces(line, search_string):

    # I assume that the search_string is in line
    # so I do not check for ind<0

    ind = string.find(line.lower(), search_string)

    # there are no spaces
    if (ind == 0):
        only_space = True
        return only_space

    # search the contents of line up to the search_string
    # to ensure there is only space between the left most 
    # column and the use statement:
    # Allowed:
    #   program hello
    #             use this_module
    # Looking to avoid:
    #    write(*,*) 'We make use of the Runge-Kutta Method'
    # and having the program say it uses the module 'of'

    i=0
    while (i<ind):

        # found a non-space character
        if not( (line[i] == " ") or (line[i] == "\t")) :
            only_space = False
            return only_space

        i = i + 1

    # each character before the 'use' is a space character (as required)
    only_space = True

    return only_space
 

#####################################################################
#  Define a usage program
#####################################################################
def usage():

    print("\nPython script to generate a list of dependencies\n")
    print("\tCurrent version only supports Fortran language")
    print("\t  using Fortran 95 as the template for module use\n")
    print("Usage:\n")
    print("    ./dependencies.py [options] <list-of-files>\n")
    print("    The options must appear before the list of files,")
    print("      otherwise the code will not work properly\n")
    print("    Supported Fortran file extensions:")
    print("      .f90, .F90, .f95, .F95, .f03, .F03, .f08,")
    print("      .F08, .ftn, .FTN, .f, .F\n")
    print("    Restrictions:")
    print("           -only one 'use' statement per line")
    print("           -only one module per 'use' statement")
    print("           -functions must be contained in a module")
    print("              or be internal functions\n")
    print("    <list-of-files>        Space separated list of the")
    print("                             files to parse\n")
    print("    -o <output>            Set output filename to 'output'")
    print("      --output=<output>\n")
    print("    -p <prefix>            Add a prefix to each object file")
    print("      --prefix=<prefix>\n")
    print("    -s <skip>              Do not check for dependencies on")
    print("      --skip-mods=<skip>     modules contained in <skip>\n")
    print("    -n, --no-msgs         Will not print any messages to")
    print("                             screen. For use when inside")
    print("                             a makefile\n")
    print("    -h, --help             Display this help message\n")

#####################################################################
#  If called as a command line program, this serves as the main prog
#####################################################################
if __name__ == "__main__":

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:p:ns:",
                        ["help","output=","prefix=","no-msgs", "skip-mods="])

    except getopt.GetoptError:

        print("\n---ERROR: Unknown Command Line Option---\n")
        usage()
        sys.exit(2)

    # defaults
    output = "fortran.dependencies"
    prefix = ""
    skip_mods = ""
    print_msgs = True

    for opt, arg in opts:

        if opt in ("-h", "--help"):
           usage()
           sys.exit(2)
        elif opt in ("-o", "--output"):
           output = arg
        elif opt in ("-p", "--prefix"):
           prefix = arg
        elif opt in ("-s", "--skip-mods"):
           skip_mods = arg
        elif opt in ("-n", "--no-msgs"):
           print_msgs = False

    # the rest of args are the files to include
    files = args[:]

    # call main program with appropriate arguments
    main_program(output, prefix, skip_mods, files, print_msgs)

